﻿using Rug.Osc;
using System;

namespace XboxToOsc
{
    class PlaybackFader : Playback
    {
        private const int SPEED_FACTOR = 4;

        public PlaybackFader(int playbackNr, OSCOutputController controller) : base(playbackNr, controller) { }

        public void changeValue(float speed)
        {
            var new_value = value + speed * SPEED_FACTOR;
            if (new_value < 1)
                new_value = 1;
            setValue(new_value);
        }

        protected override void sendOscMessage(float value)
        {
            var address = "/Mx/fader/42" + playbackNr + "3";
            var oscMsg = new OscMessage(address, (int) value);
            controller.send(oscMsg);
        }
    }
}
