﻿using Rug.Osc;
using System;
using System.Net;
using System.Threading;

namespace XboxToOsc
{
    class OSCOutputController
    {
        private XInputController xInputController;
        private OscSender sender;
        private OscReceiver receiver;

        private OscAddressManager addressManager;

        public int PlaybackBank;

        public bool debug = false;

        public OSCOutputController(String ipAddress, int send_port, int receive_port, XInputController xInputController)
        {
            this.xInputController = xInputController;

            var address = IPAddress.Parse(ipAddress);
            sender = new OscSender(address, send_port);
            sender.Connect();

            receiver = new OscReceiver(receive_port);
            Thread thread = new Thread(new ThreadStart(ListenLoop));

            receiver.Connect();
            thread.Start();

            addressManager = new OscAddressManager();
            addressManager.Attach("/Mx/label/4401/text", new OscMessageEvent(receiveBank));
        }

        private void receiveBank(OscMessage message)
        {
            PlaybackBank = Int32.Parse((string) message[0]);
        }

        public void setBank(int bank)
        {
            while (PlaybackBank != bank)
            {
                if (PlaybackBank < bank)
                {
                    send(new OscMessage("/Mx/scroll/4110/down", 1f));
                } else if (PlaybackBank > bank)
                {
                    send(new OscMessage("/Mx/scroll/4110/up", 1f));
                }
                Thread.Sleep(100);
            }
        }

        public void send(OscMessage message)
        {
            if (debug)
                Console.WriteLine("Sent: " + message.ToString());
            sender.Send(message);
        }

        public void disconnect()
        {
            sender.Dispose();
            receiver.Dispose();
        }
        private void ListenLoop()
        {
            try
            {
                while (receiver.State != OscSocketState.Closed)
                {
                    // if we are in a state to recieve
                    if (receiver.State == OscSocketState.Connected)
                    {
                        // get the next message 
                        // this will block until one arrives or the socket is closed
                        OscPacket packet = receiver.Receive();

                        // Write the packet to the console 
                        if (debug)
                            Console.WriteLine("Received: " + packet.ToString());

                        addressManager.Invoke(packet);

                        // DO SOMETHING HERE!
                    }
                }
            }
            catch (Exception ex)
            {
                // if the socket was connected when this happens
                // then tell the user
                if (receiver.State == OscSocketState.Connected)
                {
                    Console.WriteLine("Exception in listen loop");
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
