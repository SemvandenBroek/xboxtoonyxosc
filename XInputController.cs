﻿using SharpDX.XInput;
using System;
using System.Drawing;

namespace XboxToOsc
{
    class XInputController
    {
        Controller controller;
        Gamepad gamepad;

        public int deadband = 3500;
        public Point leftThumb, rightThumb = new Point(0, 0);
        public float leftTrigger, rightTrigger;

        public bool A, B, X, Y;
        

        public XInputController()
        {
            controller = new Controller(UserIndex.One);
        }

        public override string ToString()
        {
            return base.ToString() + ": Left thumb: " + leftThumb + " Right thumb: " + rightThumb + " leftTrigger: " + leftTrigger + " rightTrigger: " + rightTrigger + " A: " + A + " B: " + B + " X: " + X + " Y: " + Y;
        }

        public void Update()
        {
            if (!controller.IsConnected)
                return;

            gamepad = controller.GetState().Gamepad;

            leftThumb.X = (int)((Math.Abs((float)gamepad.LeftThumbX) < deadband) ? 0 : (float)gamepad.LeftThumbX / short.MinValue * -byte.MaxValue);
            leftThumb.Y = (int)((Math.Abs((float)gamepad.LeftThumbY) < deadband) ? 0 : (float)gamepad.LeftThumbY / short.MinValue * -byte.MaxValue);
            rightThumb.X = (int)((Math.Abs((float)gamepad.RightThumbX) < deadband) ? 0 : (float)gamepad.RightThumbX / short.MinValue * -byte.MaxValue);
            rightThumb.Y = (int)((Math.Abs((float)gamepad.RightThumbY) < deadband) ? 0 : (float)gamepad.RightThumbY / short.MinValue * -byte.MaxValue);

            leftTrigger = gamepad.LeftTrigger;
            rightTrigger = gamepad.RightTrigger;

            A = gamepad.Buttons == GamepadButtonFlags.A;
            B = gamepad.Buttons == GamepadButtonFlags.B;
            X = gamepad.Buttons == GamepadButtonFlags.X;
            Y = gamepad.Buttons == GamepadButtonFlags.Y;
        }
    }
}
