﻿
using Rug.Osc;

namespace XboxToOsc
{
    class PlaybackButton : Playback
    {
        public PlaybackButton(int playbackNr, OSCOutputController controller) : base(playbackNr, controller) { }

        protected override void sendOscMessage(float value)
        {
            var address = "/Mx/button/42" + playbackNr + "5";
            var oscMsg = new OscMessage(address, (int)value);
            controller.send(oscMsg);
        }
    }
}
