﻿namespace XboxToOsc
{
    abstract class Playback
    {
        protected int playbackNr;
        protected OSCOutputController controller;
        protected float value;

        public Playback(int playbackNr, OSCOutputController controller)
        {
            this.playbackNr = playbackNr;
            this.controller = controller;
        }
        abstract protected void sendOscMessage(float value);
        public void setValue(float new_value)
        {
            if (new_value <= byte.MaxValue && new_value >= byte.MinValue)
            {
                if (value != new_value)
                    sendOscMessage(new_value);
                value = new_value;
            }
        }
    }
}
