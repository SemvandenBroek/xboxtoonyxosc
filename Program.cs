﻿using Rug.Osc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace XboxToOsc
{
    class Program
    {
        const int BANK_NR = 8;

        static void Main(string[] args)
        {
            XInputController xInputController = new XInputController();
            OSCOutputController outputController = new OSCOutputController("192.168.2.10", 8000, 8100, xInputController);

            Console.WriteLine("Welcome to XboxToOsc");
            Console.WriteLine("Press c to exit");

            Console.WriteLine("Setting bank to " + BANK_NR);
            outputController.setBank(BANK_NR);
            
            Console.WriteLine("Ready for Playback!");

            // Add 10 faders and 10 buttons
            List<PlaybackFader> playbacks = new List<PlaybackFader>();
            List<PlaybackButton> buttons = new List<PlaybackButton>();
            for (int i = 0; i < 4; i++)
            {
                playbacks.Add(new PlaybackFader(i, outputController));
            }

            for (int i = 4; i < 15; i++)
            {
                buttons.Add(new PlaybackButton(i, outputController));
            }

            Task updateTask = Task.Run(async () =>
            { 
                while (true)
                {
                    if (Console.KeyAvailable == true)
                    {
                        ConsoleKeyInfo cki = Console.ReadKey(true);
                        if (cki.Key == ConsoleKey.C)
                        {
                            break;
                        }
                    }
                    xInputController.Update();

                    // Control 1st fader (Pan)
                    playbacks[0].changeValue(-(float)xInputController.leftThumb.X / byte.MaxValue);

                    // Control 2nd fader (Tilt)
                    playbacks[1].changeValue((float)xInputController.leftThumb.Y / byte.MaxValue);

                    // Control 3th fader (Pan 2)
                    playbacks[2].changeValue((float)xInputController.rightThumb.X / byte.MaxValue);
                    playbacks[3].changeValue((float)xInputController.rightThumb.Y / byte.MaxValue);

                    buttons[0].setValue(xInputController.A ? byte.MaxValue : 0);
                    buttons[1].setValue(xInputController.A ? byte.MaxValue : 0);
                    buttons[2].setValue(xInputController.A ? byte.MaxValue : 0);
                    buttons[3].setValue(xInputController.A ? byte.MaxValue : 0);
                    buttons[4].setValue(xInputController.A ? byte.MaxValue : 0);
                    buttons[5].setValue(xInputController.A ? byte.MaxValue : 0);

                    await Task.Delay(10);
                }

                outputController.disconnect();
            });

            updateTask.Wait();
        }

        private const int KeyPressed = 0x8000;

        public static bool IsKeyDown(Key key)
        {
            return (GetKeyState((int)key) & KeyPressed) != 0;
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern short GetKeyState(int key);
    }
}
